# KI-Lab.NRW Portal Marketplace

This repository holds projects that together comprise the Marketplace Backend and Frontend for the KI-Lab.NRW, based on Graphene.

## Developing frontend

Java http server could be used to develop the frontend. 

find the ip address of your deployment of backend and dsce in the server with: ``kubectl -n graphene get services``

Configure the ipaddresse of api and dsce in application.json to match these variables.

run the frontend with ``run-portal-fe.sh``. Now you can portforward the port you selected in application.json. Change the files in vscode and see the changes directly in your browser under http://localhost:9200/#/home.
